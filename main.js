

var cat = {

    "name" : "MeowsALot",

    "species" : "cat",

    "Foods" : {

        "likes" : [ "Tuna", "Milk", "Rat" ],

        "dislikes" : [ "Grass", "Dog's Food" ]

    }

};



//document.write("I'm " + cat.name + "! I'm a " + cat.species + ". I like to eat " + cat.Foods.likes + " and I don't like to eat " + cat.Foods.dislikes  + "<br>") ;


var animals = [

    {
        "name" : "MeowsALot",

        "species" : "Cat",

        "Foods" : {

            "likes" : [ "Tuna", "Milk", "Rat" ],

            "dislikes" : [ "Grass", "Dog's Food" ]

        }
    },

    {
        "name" : "BarksALot",

        "species" : "Dog",

        "Foods" : {

            "likes" : [ "Meat", "Bones", "Dog's Food" ],

            "dislikes" : [ "Grass", "Cat's Food", "Fruits" ]

        }
    },

    {

        "name" : "CrippingALot",

        "species" : "Bird",

        "Foods" : {

            "likes" : [ "Grass", "Grains", "Fruits" ],

            "dislikes" : [ "Meat", "Fish" ]

        }



    }


];

var i=0;

$(document).ready(function () {

    $("#nextBTN").click(function () {
        display();
        if(i==animals.length-1){
            $("#prevBTN").show();
            $("#nextBTN").hide();
            i--;
        }
        else $("#nextBTN").show();

        i++;
    });


    $("#prevBTN").click(function () {
        i--;
        display();
        if(i<=0){
            $("#prevBTN").hide();
            $("#nextBTN").show();
            i++;
        }

        else
            $("#prevBTN").show();



    });


});


function display() {
    myContent  = i+"Name : " + animals[i].name + "<br>";

    myContent += "Species : " + animals[i].species + "<br>";

    myContent += "Foods : <br>";
    myContent += "&nbsp; &nbsp; &nbsp; &nbsp;";
    myContent += "Likes : " + animals[i].Foods.likes + "<br>";
    myContent += "&nbsp; &nbsp; &nbsp; &nbsp;";
    myContent += "Dislikes : " + animals[i].Foods.dislikes + "<br>";



    $("#myDIV").html(myContent);

    $("#myDIV").css("background", "GREEN");


}



